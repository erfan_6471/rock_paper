from time import time

from constants import PLAYER_OPTION, CONTROL_OPTION, RESULT_BANNER
from core import get_play_hand, check_one_hand, modify_scores, check_total

scores = {'user': 0, 'system': 0, 'total_user': 0, 'total_system': 0}


def play_game():
	start = time()
	system_choice = get_play_hand()
	play = True
	while play:
		user_input = input(
			"Enter Your Choice Please (option are 'r' , 'p' , 's'):")
		for i in range(len(list(system_choice))):
			system_choice = next(system_choice)
		if user_input in PLAYER_OPTION.keys():
			result = check_one_hand(user_input, system_choice)
			modify_scores(result, scores)
			print(
				"your choice {} , system choice {} ,result is {} ,\t scores : {} - {} .".format(
					PLAYER_OPTION[user_input], PLAYER_OPTION[system_choice],
					RESULT_BANNER[result], scores['user'],
					scores['system']))
			check_total(scores)
		
		elif user_input in CONTROL_OPTION.keys():
			print('Bye.')
			play = False
		else:
			print("invalid input, please input again...")
	end = time()
	print(f"your time of your game is {end - start} second ")


if __name__ == '__main__':
	play_game()
