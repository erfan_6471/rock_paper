PLAYER_OPTION = {"r": "rock", "p": "paper", "s": "scissors"}
CONTROL_OPTION = {'e': 'Exit'}
PLAYER_RULE = {
	'r': {'r': 0, 'p': -1, 's': 1},
	'p': {'r': 1, 'p': 0, 's': -1},
	's': {'r': -1, 'p': 1, 's': 0}}
RESULT_BANNER = {
	1: "You win",
	0: "Draws",
	-1: "You lose"
}
